# EXT:t3content_dopetrope #

This extension provides flux content elements for the theme: Dopetrope

### Abandoned extension ###

This extension has no maintainer at the moment. If you like to get the maintainer of this extension, fill out the TYPO3.org registration form: https://extensions.typo3.org/faq/get-maintainer/

### General Information ###

* t3content_dopetrope
* v2.3.0

### How do I get set up? ###

* Install the extension from TER
* Make sure EXT:t3themes_dopetrope is installed

* Have fun with your new content elements.

### Who do I talk to? ###

* Salvatore Eckel
* salvaracer@gmx.de