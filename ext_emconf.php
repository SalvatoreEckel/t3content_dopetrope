<?php

# Extension Manager/Repository config file for ext: "t3content_dopetrope"

$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Content - Dopetrope',
    'description' => 'Provides flux fluid content elements for the t3cms theme dopetrope.',
    'category' => 'templates',
    'author' => 'Salvatore Eckel',
    'author_email' => 'salvaracer@gmx.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '2.3.0',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.18-9.99.99',
        ],
        'conflicts' => [],
        'suggests' => [
            't3themes_dopetrope' => '2.3.0',
        ],
    ],
];
